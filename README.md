# veewee definitions

This repository is mainly focused on [Gentoo](http://www.gentoo.org/)-like systems.

Our goal is to provide useful, DRY, readable and easy to fix definitions.


We are open to contributing back to [veewee](https://github.com/jedi4ever/veewee/) or [bento](https://github.com/opscode/bento) as the definitions reach a certain level of quality.

## gentoo-latest-amd64

Current vagrant box: [lxmx\_gentoo-2013.05\_chef-11.4.4](
http://lxmx-vm.s3.amazonaws.com/vagrant/boxes/lxmx_gentoo-2013.05_chef-11.4.4.box).

## funtoo-latest-amd64
Current vagrant box: [lxmx\_funtoo-2013.06\_chef-11.4.4](
http://lxmx-vm.s3.amazonaws.com/vagrant/boxes/lxmx_funtoo-2013.06_chef-11.4.4.box).
